.. pyFormation documentation master file, created by
   sphinx-quickstart on Fri Jan  2 08:44:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvevue sur la page de documentation du projet pyFormation
============================================================

C'est la page d'accueil du projet pyFormation. Ce projet contient :
    * La documentation Sphinx
    * Des fichiers RST
    * Les fichiers Python

Documentation
-------------

.. toctree::
   :maxdepth: 2
   
   doc_technique
