Documentation technique
=======================

.. automodule:: app
    :members:

.. automodule:: gestionPostgreSql
.. autoclass:: GestionBd
    :members:
    :private-members:
    :special-members:
    
.. automodule:: gestionSession
.. autoclass:: Session
    :members:
    :private-members:
    :special-members:
    
.. automodule:: gestionErreurs
    :members:
