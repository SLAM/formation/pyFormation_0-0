L'application pyFormation repose sur le contexte de formation de la SNCF,
déjà abordé dans la progression lors du module de découverte de l'exploitation des données.

## Objectifs

Travailler autour des bases de la programmation, la documentation technique
et l'accès aux bases de données en Python.

## La mission

Suite à des erreurs signalées par plusieurs agents stagiaires,
le responsable des formations, observe qu’actuellement la saisie
d'une nouvelle action de formation n’est pas toujours fiable,
il souhaite que l’application soit mise à jour pour qu’elle vérifie
la vraisemblance des informations saisies par les utilisateurs.

## Environnement technique

L'application reposant sur une base de données PostgreSql, il est bien
sûr nécessaire d'installer ce service. L'accès à la base se configure
dans le fichier `config.py`.

Sur une distribution GNU/Linux Debian, l'installation des paquets
suivants est nécessaire:

`apt -y install python3-psycopg2 python3-dateutil`

Afin de travailler sur le respect des normes et standards, propres à
Python (PEP 8) et la documentation technique :

`apt -y install python3-pycodestyle python3-sphinx python3-stemmer`

## Documentation technique

L'application pyFormation dispose d'une documentation technique qui a été
générée avec Sphinx, elle est accessible [en ligne][doc]
(via les pages de framagit et l'intégration continue).

[doc]: https://slam.frama.io/formation/pyFormation_0-0
