"""
    pyFormation.gestionSession
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Le module implémente la gestion du cas d'utilisation des sessions.

    :copyright: (c) 2015 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
"""
import config
import dict_fr as lang
import gestionErreurs as valid
from app import bdd, efface_ecran


class Session(object):
    """Définition d'une gestion de sessions"""

    def __init__(self):
        """Constructeur de Session"""
        self.numActivite = None
        self.codeAction = None
        self.viderLesErreurs()
        efface_ecran()
        while self.numActivite is None:
            self.numActivite = self.choisirUneActivite()
        self.codeAction = self.choisirUneAction()

    def viderLesErreurs(self):
        """
            Ré-initialise la liste de gestion des erreurs

            :return: Pas de valeur de retour
            :rtype: None
        """
        self.lesErreurs = []

    def ajouterErreur(self, uneErreur):
        """
            Ajoute une erreur à la liste des erreurs.

            :param uneErreur: nouvelle erreur à ajouter
            :type uneErreur: chaîne de caractères

            :return: Pas de valeur de retour
            :rtype: None
        """
        self.lesErreurs.append(uneErreur)

    def listerLesErreurs(self):
        """
            Affiche les différents éléments de la collection des erreurs.

            :return: Pas de valeur de retour
            :rtype: None
        """
        for uneErreur in self.lesErreurs:
            print(config.FAIL, " - {}".
                  format(uneErreur, config.ENDC))

    def listerLesActivites(self):
        """
            Recherche le jeu des enregistrements des activités,
            puis les affiche.

            :return: Pas de valeur de retour
            :rtype: None
        """
        lesActivites = bdd.obtenirActivites()
        for uneActivite in lesActivites:
            print(config.OKBLUE, "{} - {}".
                  format(uneActivite['numero'], uneActivite['libelle']),
                  config.ENDC)

    def listerLesActions(self):
        """
            Recherche le jeu des enregistrements des actions
            de formation, puis les affiche.

            :return: Pas de valeur de retour
            :rtype: None
        """
        lesActions = bdd.obtenirActions(self.numActivite)
        for uneAction in lesActions:
            print(config.OKBLUE, "{} - {}".
                  format(uneAction['code'], uneAction['intitule']),
                  config.ENDC)

    def listerLesSessions(self):
        """
            Recherche le jeu des enregistrements des sessions,
            puis les affiche.

            :return: Pas de valeur de retour
            :rtype: None
        """
        lesSessions = bdd.obtenirSessions(self.codeAction)
        for uneSession in lesSessions:
            print(config.OKBLUE, "| {}\t| {} |".
                  format(uneSession['numero'], uneSession['nomLieu']),
                  config.ENDC)

    def listerLesLieux(self):
        """
            Recherche le jeu des enregistrements des lieux,
            puis les affiche.

            :return: Pas de valeur de retour
            :rtype: None
        """
        lesLieux = bdd.obtenirLieux()
        for unLieu in lesLieux:
            print("{} - {}".format(unLieu['id'], unLieu['libelle']))

    def choisirUneActivite(self):
        """
            Vue permettant de choisir une activité, parmi la liste
            des activités affichées.

            :return: Renvoie le numéro d'activité ou None en
                cas d'erreur de saisie
            :rtype: entier
        """
        self.listerLesActivites()
        try:
            return int(input(lang.dico['choixActivite']))
        except ValueError:
            strErreur = "choisirUneActivite : "
            strErreur += lang.dico['errNombre']
            self.ajouterErreur(strErreur)

    def choisirUneAction(self):
        """
            Vue permettant de choisir une action de formation, parmi
            la liste des actions affichés.

            :return: Renvoie le code de l'action saisie
            :rtype: chaîne de caractères
        """
        efface_ecran()
        self.listerLesActions()
        return input(lang.dico['choixAction'])

    def choisirUnLieu(self):
        """
            Vue permettant de choisir un lieu, parmi la liste
            des lieux affichés.

            :return: Renvoie l'identifiant du lieu ou None en
                cas d'erreur de saisie
            :rtype: entier
        """
        self.listerLesLieux()
        try:
            return int(input(lang.dico['choixLieu']))
        except ValueError:
            strErreur = "choisirUnLieu : "
            strErreur += lang.dico['errNombre']
            self.ajouterErreur(strErreur)

    def choisirUneSession(self):
        """
            Vue permettant de choisir une session, parmi la liste
            des sessions affichées.

            :return: Renvoie l'identifiant de la session ou None en
                cas d'erreur de saisie
            :rtype: entier
        """
        efface_ecran()
        self.listerLesSessions()
        try:
            numero = int(input(lang.dico['choixSession']))
            return numero
        except ValueError:
            strErreur = "choisirUneSession : "
            strErreur += lang.dico['errNombre']
            self.ajouterErreur(strErreur)

    def ajouterUneSession(self, dicParam):
        """
            Vue permettant d'ajouter une nouvelle session.

            :param dicParam: paramètres issus de la base
            :type dicParam: dictionnaire

            :return: Renvoie l'enregistrement à ajouter ou None,
                en cas d'erreur de saisie
            :rtype: tuple
        """
        efface_ecran()
        try:
            codeAction = self.codeAction
            idLieu = None
            while idLieu is None:
                idLieu = self.choisirUnLieu()
            nombreMaxParticipant = \
                int(input(lang.dico['choixNumMaxParticipant']+" : "))
            dateSession = input(lang.dico['choixDateSession']+" : ")
            coutPrevu = float(input(lang.dico['choixCoutPrevu']+" : "))
            forfaitJournalier = dicParam['forfaitJournalier']
            distanceMin = dicParam['distanceMin']
            return codeAction, idLieu, nombreMaxParticipant, dateSession,\
                coutPrevu, forfaitJournalier, distanceMin
        except ValueError:
            strErreur = "ajouterUneSession : "
            strErreur += lang.dico['errNombre']
            self.ajouterErreur(strErreur)

    def modifierUneSession(self, dicSession):
        """
            Vue permettant de modifier une session.

            :param dicSession: enregistrement correspondant à
                la session à modifier
            :type dicSession: dictionnaire

            :return: Renvoie l'enregistrement modifié ou None,
                en cas d'erreur de saisie
            :rtype: tuple
        """
        efface_ecran()
        try:
            # attributs non proposés à la modification
            numero = dicSession['numero']
            codeAction = self.codeAction
            # choix idLieu
            idLieu = None
            while idLieu is None:
                idLieu = self.choisirUnLieu()
            # choix nombreMaxParticipant
            strChoix = lang.dico['choixNumMaxParticipant']
            strChoix += " ({}) : ".format(dicSession['nombreMaxParticipant'])
            nombreMaxParticipant = int(input(strChoix))
            # choix dateSession
            strChoix = lang.dico['choixDateSession']
            strChoix += " ({}) : ".format(dicSession['dateSession'])
            dateSession = input(strChoix)
            # choix coutPrevu
            strChoix = lang.dico['choixCoutPrevu']
            strChoix += " ({}) : ".format(dicSession['coutPrevu'])
            coutPrevu = float(input(strChoix))
            # choix coutReel
            strChoix = lang.dico['choixCoutReel']
            strChoix += " ({}) : ".format(dicSession['coutReel'])
            coutReel = float(input(strChoix))
            # retour
            return idLieu, nombreMaxParticipant, dateSession,\
                coutPrevu, coutReel, numero
        except ValueError:
            strErreur = "modifierUneSession : "
            strErreur += lang.dico['errNombre']
            self.ajouterErreur(strErreur)

    def run(self):
        """
            Boucle principal permettant de gérer le cas
            d'utilisation des sessions.

            :return: Pas de valeur de retour
            :rtype: None
        """
        while True:
            efface_ecran()
            print(config.OKBLUE, lang.dico['listeSession'].
                  format(self.numActivite, self.codeAction), sep='')
            self.listerLesSessions()
            print(config.ENDC)
            self.listerLesErreurs()
            self.viderLesErreurs()
            print(config.HEADER, lang.dico['menuSession'], config.ENDC, end='')
            try:
                choix = int(input())
                if choix == 1:
                    pass
                elif choix == 2:
                    dicParam = bdd.obtenirParametres()
                    tupSession = None
                    while tupSession is None:
                        tupSession = self.ajouterUneSession(dicParam)
                    if valid.verifierDonneesSessionC(self.lesErreurs,
                       tupSession):
                        if not bdd.creerSession(tupSession):
                            # erreur création Bdd
                            strErreur = lang.dico['errCreerSession']
                            self.ajouterErreur(strErreur)
                elif choix == 3:
                    numSession = None
                    while numSession is None:
                        numSession = self.choisirUneSession()
                    dicSession = bdd.detailSession(numSession)
                    tupSession = None
                    while tupSession is None:
                        tupSession = self.modifierUneSession(dicSession)
                    if valid.verifierDonneesSessionM(numSession,
                       self.lesErreurs, tupSession):
                        if not bdd.modifierSession(tupSession):
                            # erreur maj Bdd
                            strErreur = lang.dico['errModifSession']
                            self.ajouterErreur(strErreur)
                elif choix == 4:
                    numSession = None
                    while numSession is None:
                        numSession = self.choisirUneSession()
                    if not bdd.supprimerSession(numSession):
                        strErreur = lang.dico['errSupprSession']
                        self.ajouterErreur(strErreur)
                else:
                    break
            except Exception as err:
                print(config.FAIL, lang.dico['err'], ":",
                      str(err), config.ENDC)
