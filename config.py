DB_NAME = "pyFormation"     # nom de la base de données
USER = "moulinux"           # propriétaire ou utilisateur
PASSWD = "secret"           # mot de passe d'accès
HOST = "localhost"          # nom ou adresse IP du serveur

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
