"""
    pyFormation.gestionErreurs
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Le module implémente la gestion des erreurs.

    :copyright: (c) 2015 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
"""
import dict_fr as lang
from app import bdd


def verifierDonneesSessionM(numSession, dicoErr, tupSession):
    """
        Fonction qui vérifie la saisie lors de la modification d'une session.
        Pour chaque champ non valide,
        un message est ajouté à la liste des erreurs.

        :param numSession: identifiant de la session
        :type numSession: entier

        :param dicoErr: dictionnaire des erreurs
        :type dicoErr: dict

        :param tupSession: paramètres de la session modifiée
        :type tupSession: tuple

        :return: True si la validation a réussie, False sinon
        :rtype: booléen
    """
    estValide = True
    # on recherche le nombre d'agents déjà inscrits à la session
    nbInscrits = bdd.trouverNbInscrits(numSession)
    if tupSession[1] < nbInscrits:
        # le nombre max de participants saisie est trop petit
        strErreur = lang.dico['errNbInscrits'].format(nbInscrits)
        dicoErr.append(strErreur)
        estValide = False
    return estValide


def verifierDonneesSessionC(dicoErr, tupSession):
    """
        Fonction qui vérifie la saisie lors de la création d'une session.
        Pour chaque champ non valide,
        un message est ajouté à la liste des erreurs.

        :param dicoErr: dictionnaire des erreurs
        :type dicoErr: dict

        :param tupSession: paramètres de la nouvelle session
        :type tupSession: tuple

        :return: True si la validation a réussie, False sinon
        :rtype: booléen
    """
    estValide = True
    # ToDo

    return estValide
