from cx_Freeze import setup, Executable

# Les dépendances sont automatiquement détectées.
# On configure les fichiers à inclure
build_exe_options = {"packages": ["psycopg2"], }

# On appelle la fonction setup
setup(
    name="pyFormation",
    version="0.1",
    description="""Gestionnaire de sessions de formation.""",
    options={"build_exe": build_exe_options},
    executables=[Executable("app.py")],
)
