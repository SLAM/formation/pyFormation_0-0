"""
    pyFormation.app
    ~~~~~~~~~~~~~~~

    Module principal de l'application de gestion des formations.

    :copyright: (c) 2015 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
"""
from gestionPostgreSql import *
import dict_fr as lang
import config
import os
import sys

if len(sys.argv) == 3:
    config.DB_NAME = sys.argv[1]
    config.USER = sys.argv[2]
bdd = GestionBd()


def efface_ecran():
    """
        Efface l'écran de la console.

        :return: Pas de valeur de retour
        :rtype: None
    """
    if sys.platform.startswith("win"):
        # Si système Windows
        os.system("cls")
    else:
        # Si système GNU/Linux ou OS X
        os.system("clear")


# Programme principal
if __name__ == '__main__':
    if bdd.connBase():
        sys.exit()
    from gestionSession import *
    efface_ecran()
    while True:
        print(config.HEADER, lang.dico['menuApp'], config.ENDC, end='')
        try:
            choix = int(input())
            if choix == 1:
                Session().run()
            elif choix == 2:
                pass
            else:
                bdd.valider()
                bdd.fermer()
                break
        except Exception as err:
            print(config.FAIL, lang.dico['err'], ":", str(err), config.ENDC)
        else:
            efface_ecran()
