-- suppression des tables
DROP TABLE IF EXISTS "Parametre";
DROP TABLE IF EXISTS "Distance";
DROP TABLE IF EXISTS "Inscription";
DROP TABLE IF EXISTS "Session";
DROP TABLE IF EXISTS "Action";
DROP TABLE IF EXISTS "Agent";
DROP TABLE IF EXISTS "Lieu";
DROP TABLE IF EXISTS "Activite";

-- création des tables
CREATE TABLE "Activite" (
  "numero" INTEGER,
  "libelle" VARCHAR(20) NOT NULL,
  PRIMARY KEY("numero")
);

CREATE TABLE "Lieu" (
  "id" INTEGER,
  "libelle" VARCHAR(32) NOT NULL,
  "codePostal" CHAR(5) NOT NULL,
  "telephone" CHAR(10) NULL,
  "typeLieu" CHAR(1) NOT NULL,
  PRIMARY KEY("id")
);

CREATE TABLE "Agent" (
  "code" CHAR(8),
  "idAdminLieu" INTEGER NOT NULL,
  "civilite" CHAR(3) NOT NULL,
  "prenom" VARCHAR(32) NOT NULL,
  "nom" VARCHAR(32) NOT NULL,
  "adresse1" VARCHAR(40) NULL,
  "adresse2" VARCHAR(32) NULL,
  "codePostal" CHAR(5) NULL,
  "ville" VARCHAR(32) NULL,
  "dateNaissance" DATE NOT NULL,
  "dateEmbauche" DATE NOT NULL,
  PRIMARY KEY("code"),
  FOREIGN KEY("idAdminLieu") REFERENCES "Lieu"("id")
);

CREATE TABLE "Action" (
  "code" CHAR(5),
  "numeroActivite" INTEGER NOT NULL,
  "intitule" VARCHAR(255) NOT NULL,
  "cout" INTEGER NOT NULL,
  "duree" INTEGER NOT NULL,
  "dateCreation" DATE NOT NULL,
  PRIMARY KEY("code"),
  FOREIGN KEY("numeroActivite") REFERENCES "Activite"("numero")
);

CREATE TABLE "Session" (
  "numero" SERIAL,
  "codeAction" CHAR(5) NOT NULL,
  "idLieu" INTEGER NOT NULL,
  "nombreMaxParticipant" INTEGER NOT NULL,
  "dateSession" DATE NOT NULL,
  "coutPrevu" DECIMAL(7,2) NOT NULL,
  "coutReel" DECIMAL(7,2) NULL,
  "forfaitJournalier" DECIMAL(6,2) NOT NULL,
  "distanceMin" INTEGER NOT NULL,
  PRIMARY KEY("numero"),
  FOREIGN KEY("idLieu") REFERENCES "Lieu"("id"),
  FOREIGN KEY("codeAction") REFERENCES "Action"("code")
);

CREATE TABLE "Inscription" (
  "numeroSession" INTEGER,
  "codeAgent" CHAR(8) NOT NULL,
  "presence" INTEGER NOT NULL default '0',
  "fraisHebergement" DECIMAL(6,2) NULL,
  PRIMARY KEY ("numeroSession", "codeAgent"),
  FOREIGN KEY("codeAgent")  REFERENCES "Agent"("code"),
  FOREIGN KEY("numeroSession")  REFERENCES "Session"("numero")
);

CREATE TABLE "Distance"(
  "idLieuFormation" INTEGER NOT NULL,
  "idLieuAdministratif" INTEGER NOT NULL,
  "distanceEntreLieu" INTEGER NOT NULL,
  PRIMARY KEY ("idLieuFormation", "idLieuAdministratif"),
  FOREIGN KEY("idLieuFormation")  REFERENCES "Lieu"("id"),
  FOREIGN KEY("idLieuAdministratif")  REFERENCES "Lieu"("id")
);

CREATE TABLE "Parametre"(
  "id" SERIAL,
  "forfaitJournalier" DECIMAL(6,2) NOT NULL,
  "distanceMin" INTEGER NOT NULL,
  PRIMARY KEY ("id")
);
