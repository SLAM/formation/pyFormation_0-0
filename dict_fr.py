dico = {
    'menuApp': """\
Que voulez-vous faire :
 1) Gérer les sessions
 2) Gérer les inscriptions
 3) Quitter l'application ?        Votre choix :\
""",
    'menuSession': """\
Que voulez-vous faire :
 1) Lister les sessions
 2) Ajouter une nouvelle session
 3) Modifier une session existante
 4) Supprimer une session existante
 5) Terminer ?        Votre choix :\
""",
    'err': "Erreur",
    'errCreerSession': "La nouvelle session n'a pas pu être crée",
    'errModifSession': "La session n'a pas pu être modifiée",
    'errSupprSession': "La session n'a pas pu être supprimée",
    'errNbInscrits': "Le nombre maximum d'inscrits doit rester supérieur à {}",
    'errNombre': "Veuillez saisir un nombre !",
    'choixActivite': "Choisir une activité : ",
    'choixAction': "Choisir une action : ",
    'choixLieu': "Choisir un lieu : ",
    'choixSession': "Choisir le numéro de session : ",
    'listeSession': """\
##################################
Activité\t: {}
Action\t\t: {}
##################################
Liste des sessions correspondantes :\
""",
    'choixNumSession': "Saisissez le numéro de session",
    'choixNumMaxParticipant': "Saisissez le nombre maximal de participants",
    'choixDateSession': "Saisissez la date de session",
    'choixCoutPrevu': "Saisissez le coût prévu",
    'choixCoutReel': "Saisissez le coût réel",
    'choixForfaitJournalier': "Saisissez le forfait journalier",
    'choixDistanceMin': "Saisissez la distance minimal",
}
